const express = require('express'); 

const route = express.Router(); 

const { userCollection } = require('../data/userCollection.js');

route.get('/', (req, res) => {
   res.send(usersCollection);
});


route.get('/profile', (req, res) => {
    res.send('Welcome to the User Profile');
});

//Practice sending a post method request in order to 'create a new user'. 

   //if we wish to send a 'post' request we need to use the post(). instruct the server what to do upon receiving the request and upon sending the response. 
   route.post('/create', (req, res) => {
       //validate whether we can catch the contents of the request body. 
       let body = req.body;
       console.log(body); 
       let fName = body.firstName;
       let lName = body.lastName;
       let age = body.age; 
       //validate also if the fields are populated. create a control structure that will make sure that all fields are properly populated. 
       // '!' => NOT
       // if(fName === true)
       //if( fName === '' && lName === '' )
     if ( fName && lName && age) {
          //value if true
            //save the new data inside the users collection. 
          usersCollection.push(body); 
          //we need to give a response back to the client to confirm 
          res.send(`Account for ${fName} has been created`);
          //we need to give a response back to the client to confirm

          //How to inject a status code in the response. 
            //1. insert a 2nd parameter on the send(). 
            //2. status() => to set the status code
          res.status(201).send(`Account for ${fName} has been created`);
     } else {
          //value if false
          res.send('Check your input, All fields are Required!'); 
          // status code? client (4)
          // Bad Request (400)
          res.status(400).send('Check your input, All fields are Required!'); 
          //send() -> will allow us to send data back to the client and end the transmission when data is sent.
          //send a string response in format other than JSON. (XML, CSV, Plain Text)
     }
   })

module.exports = route; 
