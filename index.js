const express = require('express'); 

const server = express(); 

const userRoute = require('./routes/userRoutes.js');

//In order for our app to recognize the format of our request structure, we need to apply the following changes in our settings. 

  //1. Lets allow our app to read information/data structures that are in json format. 
server.use(express.json()); 


//'Required' to have an initial endpoint? No
//'Recommended' ? Yes
//...why? to describe the collection of routes.
//so we can differentiate it from all other routes. so that is for the routes related to the users.
server.use('/users', 'userRoutes'); 

const address = 3000; 


server.listen(address, () => {
	console.log(`Server is running on port: ${address}`);
}); 
