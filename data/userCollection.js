module.exports.usersCollection = [
  {
  	"firstName": "John",
  	"lastName": "Doe",
  	"age": "35"
  },
  {
  	"firstName": "Maria",
  	"lastName": "DeBomba",
  	"age": "28"
  },
  {
  	"firstName": "Juan",
  	"lastName": "Tamad",
  	"age": "32"
  }
];
